import os
import json
import sys

def traverse(obj):
	"""

	This function is used to traverse the given JSON object recursively 
	and add the version attribute to it
	
	Parameters 
	-------------------
	obj :  The JSON to be traversed i.e a Python JSON object
	
	"""
	if (isinstance(obj, list)):
		for i in obj:
			traverse(i)
	elif (isinstance(obj,dict)):
		if (obj["type"]=="directory"):
			traverse(obj["contents"])
		elif (obj["type"]=="file"):
			obj["version"] = obj["name"]

if (__name__ == "__main__"):
	
	# Executing a bash script to create the directory structure 
	# as a JSON using UNIX utility Tree in out.txt
	
	t = os.system("bash create.sh")
	if (t!=0):
		# Needs to run as ROOT if "tree" is absent
		print("Use sudo python3 solution.py")
		sys.exit(1)

	# Reading the JSON 
	try : 
		with open("out.txt","r") as f:
			data = json.loads(f.read())
	except : 
		print("Some Error occured")			

	traverse(data) #traversing the JSON 
	
	# Writing the JSON
	try : 
		with open("out.txt","w") as f:
			f.write(json.dumps(data,indent = 2))	
	except :
		print("Error occured")