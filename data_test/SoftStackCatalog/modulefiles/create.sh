# x is used for checking if the "tree" utility is present or not
x=$(dpkg -l | grep "\bdirectory tree\b");
if [[ "$x" == "" ]]; then
	# ROOT permissions required to install tree
	if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root" 
    exit 1
	fi
	echo "----------------------
Installing tree
--------------------"
	apt-get install tree -y;
fi
tree -J mugqic > out.txt ; # I/O Redirection
echo "-------------------------------
JSON output created in out.txt 
-------------------------------";